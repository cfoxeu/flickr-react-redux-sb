import React from 'react'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router-dom'

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from '../src/reducers';

require('es6-promise').polyfill();
require('isomorphic-fetch');

import renderFullPage from '../src/renderFullPage'
import App from '../src/App';

export default function router(req, res) {
    let context = {}
        // Set default request params
        const baseUrl = 'https://api.flickr.com/services/feeds/photos_public.gne'
        const urlParams = [
            'safe_search=1',
            'format=json',
            'nojsoncallback=1',
            // 'tags=coffee,nature,green',
            // 'tag_mode=any',
        ]
        // Join baseUrl with ?urlParams separated by &
        const url = `${baseUrl + (urlParams ? '?' + urlParams.join('&') : '')}`
        // Perform fetch and render static html
        fetch(url)
        .then(res => res.json())
        .then(json => {
            let data = { content: json }
            const html = renderToString(
                <Provider store={createStore(reducers)}>
                    <StaticRouter context={context} location={req.url}>
                        <App content={data.content} />
                    </StaticRouter>
                </Provider>
            )
            res.status(200).send(renderFullPage(html, data))
        })
        .catch((err) => console.log('Error: ', err))
    }
