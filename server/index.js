import app from './app'

const port = process.env.PORT || 3000;

app.listen(port, '0.0.0.0', (err) => {
	if(err) {
		console.error(err);
	} else {
		console.info(`listening on ${port}`);
	}
});
