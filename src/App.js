import React, { Component } from 'react'
import {
    Route,
    Switch
} from 'react-router-dom'

import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'
import Home from './components/Home/Home'
import NoMatch from './components/NoMatch/NoMatch'

export default class App extends Component {
    render() {
        return (
            <div className="page-wrapper">
                <Header/>
                <Switch>
                    <Route exact path="/" render={() => { return <Home content={this.props.content} /> }} />
                    <Route component={NoMatch}/>
                </Switch>
                <Footer/>
            </div>
        )
    }
}
