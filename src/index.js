import React from 'react';
import { hydrate } from 'react-dom';
import { createBrowserHistory } from 'history';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import App from './App';
import reducers from './reducers';

const history = createBrowserHistory();
const store = createStore(reducers, applyMiddleware(routerMiddleware(history), thunk));

hydrate(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App data={window.__PRELOADED_STATE__}/>
    </ConnectedRouter>
  </Provider>
, document.getElementById('app'));

if(process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept();
  module.hot.accept('./reducers', () => {
    store.replaceReducer(require('./reducers').default);
  });
}
