export const buildTagList = list => {
  return {
      type: 'SEARCH',
      payload: list
  }
}
export const setSearchPhrase = searchPhrase => {
    return {
        type: 'GET_SEARCH_PHRASE',
        payload: searchPhrase
    }
}
export const getNewList = newList => {
    return {
        type: 'GET_NEW_LIST',
        payload: newList
    }
}
