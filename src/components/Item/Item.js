import React, { Component } from 'react'

export default class Item extends Component {
    render() {
        const {
            link,
            media,
            title,
            author,
            author_id,
            published,
            tags,
        } = this.props
        // Create author profile link // Defaults to /photos
        const authorProfileLink = `https://www.flickr.com/${author_id}`
        // Format published date
        const tidyString = (string, type, update = '') => {
            if (type === 'date') {
                // Create datetime string
                const dta = new Date(string).toString()
                // Remove extra fluff
                .replace(/( GMT\+0000 \(GMT\))/g, update)
                .substring(4)
                .split(' ')
                return `Published ${dta[1]} ${dta[0]} ${dta[2]} at ${dta[3]}`
            }
            if (type === 'author') {
                return string.replace(/(nobody@flickr.com |\("|"\))/g, update)
            }
            if (type === 'title') {
                if (string.length < 1) {
                    return string
                } else if (string.length > 24) {
                    return `${string.substring(0, 24)}...`
                } else {
                    return 'Untitled'
                }
            }
            if (type === 'tags') {
                if (string.length) {
                    return string.split(' ').slice(0, 12).map((tag, i) => {
                        return ( <span key={i}>{ tag }&nbsp;</span> )
                    })
                } else {
                    return 'None'
                }
            }
        } // tidyString Fn
        return (
            <article>
                <div className="item-wrapper">
                    <figure>
                        <a href={ authorProfileLink }>
                            <img className="center-block" src={ media.m } alt={ tidyString(title, 'title') }/>
                        </a>
                    </figure>
                    <figcaption>
                        <h3 className="title">{ tidyString(title, 'title') }</h3>
                        <div className="published">{ tidyString(published, 'date') }</div>
                        <div className="link-wrapper">
                            <div className="author"><a href={ authorProfileLink }>{ tidyString(author, 'author') }</a></div>
                            <a className="link" href={ link }>View on Flickr</a>
                        </div>
                        <div className="tags">Tags: { tidyString(tags, 'tags') }</div>
                    </figcaption>
                </div>
            </article>
        )
    }
}
