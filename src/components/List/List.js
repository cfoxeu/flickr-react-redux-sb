import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { buildTagsList } from '../../actions/filterActions'

import FilterList from '../FilterList/FilterList'

if(process.env.WEBPACK) require('./index.scss')

class List extends Component {

    constructor() {
        super()
        this.state = {}
        this.generateTagList = this.generateTagList.bind(this)
    }

    componentWillMount() {
        this.generateTagList(this.props.items, (err, tagList) => {
            this.setState({ generatedTagList: tagList })
        })
    }

    generateTagList(items, callback) {
        let tagList = {}
        items.map(item => {
            if (item.tags) {
                // Turn string into array of tags
                item.tags.split(' ')
                .map(tag => {
                    // Check if tag exists in tagList
                    tagList[tag]
                    // Push item to its tag array
                    ? tagList[tag].push(item)
                    // If tag doesn't exist already create it then push item to it
                    : Object.assign(tagList, {[tag]:[item]})
                })
            } else {
                // This item has no tags, just chill.
            }
        })
        callback(null, tagList)
    }

    render() {
        const {
            items,
            loading
        } = this.props
        if (loading) {
            return (
                <div>Loading...</div>
            )
        } else {
            return (
                <div className="item-list">
                    <FilterList initialItems={items} preTags={this.state.generatedTagList} />
                </div>
            )
        }
    }
}

const mapStateToProps = state => {
	return {
		items: state.data.items,
        loading: state.data.loading
	}
}

const mapDispatchToProps = dispatch => {
	return bindActionCreators({
		buildTagsList: buildTagsList
	}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(List)
