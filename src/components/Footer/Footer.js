import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <footer>
                <h5>Flickr app - Chris Fox - 2018</h5>
            </footer>
        )
    }
}
export default Footer
