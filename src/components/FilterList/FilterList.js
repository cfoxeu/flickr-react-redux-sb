import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { setSearchPhrase, getNewList } from '../../actions/filterActions'
import { _ } from 'lodash'

import Item from '../Item/Item'

class FilterList extends Component {

    componentDidMount() {
        this.props.dispatch(setSearchPhrase(''))
    }

    filterList() {
        // Reset updatedList to initialItems
        let updatedList = this.props.initialItems
        // Filter updatedList by searchPhrase
        if (this.props.searchPhrase.length) {
            updatedList = _.forEach(preTags, (item, tag) => {
                tag.toLowerCase().indexOf(searchPhrase) !== -1
                ? item
                : null
            })
        }
        this.props.dispatch(getNewList(updatedList))
    }
    updateSearchPhrase(e) {
        this.props.dispatch(setSearchPhrase(e.target.value))
    }

    render() {
        console.log(this.props)
        const {
            initialItems,
            // searchPhrase,
            newList,
            preTags
        } = this.props
        const items = newList ? newlist : initialItems
        return (
            <div>
                <select name="tags">
                    <option value="none" onSelect={(e) => this.updateSearchPhrase(e)}>Filter by available tags...</option>
                    {
                        Object.keys(preTags).map((tag, i) => {
                            return <option value={tag} key={i} onSelect={this.filterList}>{tag}</option>
                        })
                    }
                </select>
                {
                    items.map((item, i) => {
                        return <Item key={i} {...item} />
                    })
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
	return {
		searchPhrase: state.searchPhrase,
        newList: state.filtered
	}
}

const mapDispatchToProps = dispatch => {
	return bindActionCreators({
		setSearchPhrase: setSearchPhrase,
        getNewList: getNewList
	}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterList)
