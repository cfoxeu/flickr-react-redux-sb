import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { fetchData } from '../../actions/fetchData'

import List from '../List/List'

if(process.env.WEBPACK) require('./index.scss')

class Home extends Component {
	componentWillMount() {
		if (this.props.content) {
			this.props.dispatch(fetchData(this.props.content.items))
		} else {
			console.log('Waiting for data...')
		}
	}
	render() {
		return (
			<div className='home'>
				<List />
			</div>
		)
	}
}

const mapDispatchToProps = dispatch => {
	return bindActionCreators({
		fetchData: fetchData,
	}, dispatch)
}

export default connect(mapDispatchToProps)(Home)
