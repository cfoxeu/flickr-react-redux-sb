const initialState = {
    loading: true,
    items: null
}

export const fetchDataReducer = (state=initialState, action) => {
    switch(action.type) {
        case 'FETCH_DATA':
            return {
                ...state,
                loading: false,
                items: action.payload
            }
        default:
            // ALWAYS have a default case in a reducer
            return state
    }
}
