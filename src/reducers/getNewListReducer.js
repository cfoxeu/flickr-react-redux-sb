const initialState = {
    filtered: null
}

export const getNewListReducer = (state=initialState, action) => {
    switch(action.type) {
        case 'GET_NEW_LIST':
            return {
                ...state,
                filtered: action.payload
            }
        default:
            // ALWAYS have a default case in a reducer
            return state
    }
}
