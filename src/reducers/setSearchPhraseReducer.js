const initialState = {
    searchPhrase: ''
}

export const getSearchPhraseReducer = (state=initialState, action) => {
    switch(action.type) {
        case 'GET_SEARCH_PHRASE':
            return {
                ...state,
                searchPhrase: action.payload
            }
        default:
            // ALWAYS have a default case in a reducer
            return state
    }
}
