const initialState = {
    container: {}
}

export const buildTagsListReducer = (state=initialState, action) => {
    switch(action.type) {
        case 'SEARCH':
            return {
                ...state,
                container: action.payload
            }
        default:
            // ALWAYS have a default case in a reducer
            return state
    }
}
