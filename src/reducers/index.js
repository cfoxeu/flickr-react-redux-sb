import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { fetchDataReducer } from './fetchDataReducer'
import { buildTagsListReducer } from './filterByTagsReducer'
import { setSearchPhraseReducer } from './setSearchPhraseReducer'
import { getNewListReducer } from './getNewListReducer'

export default combineReducers({
    list: buildTagsListReducer,
    searchPhrase: setSearchPhraseReducer,
    newList: getNewListReducer,
    data: fetchDataReducer,
    routing: routerReducer
})
