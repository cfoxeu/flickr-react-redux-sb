# FLICKR application with React and Redux

### Install
```sh
cd flickr-react-redux-sb
npm install
```

### Run the app from a dev server
```sh
npm start
```

### Build the app
```sh
npm run build
```

### Host the app from a server (app must have been built)
```sh
npm run serve
```

### Article
About details of this boilerplate, please refer to: [React: Server-side Rendering and Hot Reloading](https://medium.com/@justinjung04/react-server-side-rendering-and-hot-reloading-ffb87ca81a89)
